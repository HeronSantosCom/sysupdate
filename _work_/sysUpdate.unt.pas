unit sysUpdate.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, ImgList, CoolTrayIcon, WebCopy, StdCtrls,
  TFlatGaugeUnit, TFlatGroupBoxUnit, dxCore, dxButton, IniFiles, WinInet;

type
  TfrmUpdate = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    btnAtualizar: TdxButton;
    btnSair: TdxButton;
    fgbStatus: TFlatGroupBox;
    fgbProgresso: TFlatGroupBox;
    fgeProgresso: TFlatGauge;
    tmrProgresso: TTimer;
    lblStatus: TLabel;
    wbcDownload: TWebCopy;
    ctiBandeja: TCoolTrayIcon;
    iclBandeja: TImageList;
    procedure wbcDownloadFileStart(Sender: TObject; idx: Integer);
    procedure wbcDownloadFileDone(Sender: TObject; idx: Integer);
    procedure FormCreate(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure tmrProgressoTimer(Sender: TObject);
  private
    { Private declarations }
  public
    function verificaConn:boolean;
    { Public declarations }
  end;

var
  frmUpdate: TfrmUpdate;
  iniSys:TiniFile;
  servidorEndereco, arquivoNome, arquivoPatch, arquivoNovoNome, arquivoNovoPatch :String;
  arquivoVersao, arquivoNovoVersao :Integer;
  arquivoRecebido :Boolean;

implementation

{$R *.dfm}

function TfrmUpdate.verificaConn:Boolean;
var
  flags: dword;
begin
  lblStatus.Caption := 'Verificando conex�o com a Internet...';
  Result := InternetGetConnectedState(@flags, 0);
  if Result then begin
    if (flags and INTERNET_CONNECTION_MODEM) = INTERNET_CONNECTION_MODEM then
      lblStatus.Caption := 'Conectado � Internet pelo modem...';
    if (flags and INTERNET_CONNECTION_LAN) = INTERNET_CONNECTION_LAN then
      lblStatus.Caption := 'Conectado � Internet pela rede...';
    if (flags and INTERNET_CONNECTION_PROXY) = INTERNET_CONNECTION_PROXY then
      lblStatus.Caption := 'Conectado � Internet via proxy...';
    if (flags and INTERNET_CONNECTION_MODEM_BUSY) =INTERNET_CONNECTION_MODEM_BUSY then
      lblStatus.Caption := 'Erro: Modem Ocupado!';
 end else lblStatus.Caption := 'Erro: Sem acesso � Internet!';
end;

procedure TfrmUpdate.wbcDownloadFileStart(Sender: TObject; idx: Integer);
begin
  ctiBandeja.CycleIcons := True;
  ctiBandeja.IconVisible := True;
  tmrProgresso.Enabled := True;
  btnAtualizar.Enabled := False;
end;

procedure TfrmUpdate.wbcDownloadFileDone(Sender: TObject; idx: Integer);
var
  arquivoCompleto :PChar;
begin
  ctiBandeja.CycleIcons := False;
  ctiBandeja.IconVisible := False;
  tmrProgresso.Enabled := False;
  fgeProgresso.Progress := 100;
  lblStatus.Caption := 'Tranfer�ncia Concluida!';
  btnAtualizar.Enabled := True;
  if arquivoRecebido = True Then
  begin
    if MessageDlg('Novo arquivo recebido com sucesso!' + #13 + 'Deseja instal�-lo?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
    begin
      lblStatus.Caption := 'Iniciando Instala��o...';
      arquivoCompleto := PChar('.\temp\' + arquivoNovoPatch);
      WinExec(arquivoCompleto,SW_SHOWNORMAL);
      lblStatus.Caption := 'Atualiza��o Conclu�da...';
      arquivoRecebido := False;
    end;
  end;
end;

procedure TfrmUpdate.FormCreate(Sender: TObject);
begin
  iniSys := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'sysUpdate.ini');
    servidorEndereco := iniSys.ReadString('SERVIDOR','ENDERECO','');
    arquivoNome := iniSys.ReadString('ARQUIVO','NOME','');
    arquivoVersao := iniSys.ReadInteger('ARQUIVO','VERSAO',0);
    arquivoPatch := iniSys.ReadString('ARQUIVO','ARQUIVO','');
  iniSys.Free;
end;

procedure TfrmUpdate.btnSairClick(Sender: TObject);
begin
  Halt;
end;

procedure TfrmUpdate.btnAtualizarClick(Sender: TObject);
begin
  if verificaConn = True then
  begin
    DeleteFile('.\temp\sysupdate.ini');
    wbcDownload.Items.Clear;
    with wbcDownload.Items.Add do
    begin
      url := servidorEndereco + 'sysupdate.ini';
      targetdir := '.\temp';
      protocol := wpHttp;
    end;
    lblStatus.Caption := 'Verificando se existe atualiza��o...';
    wbcDownload.ShowDialog := False;
    wbcDownload.Execute;
    if wbcDownload.ProgressBar.Position = 100 then
    begin
      iniSys := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'temp\sysupdate.ini');
        arquivoNovoNome := iniSys.ReadString('ARQUIVO','NOME','');
        arquivoNovoVersao := iniSys.ReadInteger('ARQUIVO','VERSAO',0);
        arquivoNovoPatch := iniSys.ReadString('ARQUIVO','ARQUIVO','');
      iniSys.Free;
      if arquivoVersao < arquivoNovoVersao then
      begin
        lblStatus.Caption := 'Nova vers�o encontrada...';
        if MessageDlg('Existe uma nova vers�o do ' + arquivoNome + '...     ' + #13 + #13 + 'Vers�o Atual: ' + IntToStr(arquivoVersao) + #13 + 'Vers�o Nova: ' + IntToStr(arquivoNovoVersao) + #13 + #13 + '� altamente recomend�vel atualizar...' + #13 + 'Efetuar atualiza��o?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
        begin
          DeleteFile('.\temp\' + arquivoNovoPatch);
          wbcDownload.Items.Clear;
          with wbcDownload.Items.Add do
          begin
            url := servidorEndereco + arquivoNovoPatch;
            targetdir := '.\temp';
            protocol := wpHttp;
          end;
          lblStatus.Caption := 'Baixando novo arquivo...';
          wbcDownload.ShowDialog := False;
          wbcDownload.ThreadExecute;
          arquivoRecebido := True;
        end;
      end else lblStatus.Caption := 'Sua vers�o j� est� atualizada!';
    end;
  end else lblStatus.Caption := 'Erro: Sem acesso � Internet!';
end;

procedure TfrmUpdate.tmrProgressoTimer(Sender: TObject);
begin
  lblStatus.Caption := wbcDownload.ProgressSizeLabel.Caption;
  fgeProgresso.Progress := wbcDownload.ProgressBar.Position;
end;

end.

